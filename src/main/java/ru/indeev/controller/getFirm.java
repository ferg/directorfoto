package ru.indeev.controller;

import ru.indeev.dao.FirmsDao;
import ru.indeev.dao.PhotosDao;
import ru.indeev.model.FirmsEntity;
import ru.indeev.model.PhotosEntity;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.MultipartConfig;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.Part;
import java.io.File;
import java.io.IOException;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by ferg on 11.04.2017.
 */
@WebServlet(name = "getFirm", urlPatterns = {"/firm"})
@MultipartConfig(fileSizeThreshold=1024*1024*10, 	// 10 MB
                    maxFileSize=1024*1024*50,      	// 50 MB
                    maxRequestSize=1024*1024*100)   // 100 MB
public class getFirm extends HttpServlet {

    private static String NEW = "/jsp/newFirmForm.jsp";
    private static String LIST = "/jsp/firmsList.jsp";
    private static String SHOW = "/jsp/showFirm.jsp";
    private static String EDIT = "/jsp/editFirm.jsp";
    private static String ERROR = "/jsp/error.jsp";
    private FirmsDao firmsDao;
    private PhotosDao photosDao;

    public getFirm() {
        super();
        this.firmsDao =  new FirmsDao();
        this.photosDao = new PhotosDao();
    }

    protected void doPost(HttpServletRequest request,
                          HttpServletResponse response) throws ServletException, IOException {

        request.setCharacterEncoding("UTF-8");

        FirmsEntity firm;
        String action = null;
        try {
            action = request.getParameter("action");
        } catch (Exception e) {
            e.printStackTrace();
        }

        if ("editFirmName".equalsIgnoreCase(action)){

            String targetName = request.getParameter("firmName");
            if (firmsDao.isExist(targetName) || firmsDao.getById(request.getParameter("id")).equals(targetName)){
                firm = firmsDao.getByName(targetName);
            } else {
                firm = firmsDao.editName(request.getParameter("id"), targetName);
            }
            request.setAttribute("firm", firm);
            request.setAttribute("fotoCount", firm.getPhotoList().size());
            RequestDispatcher dispatcher = getServletContext().getRequestDispatcher(SHOW);
            dispatcher.forward(request,response);

            return;
        } else if ("movePhoto".equalsIgnoreCase(action)){
            String targetName = request.getParameter("targetName");
            String id = request.getParameter("id");
            String[] photos = request.getParameterValues("foto");
            firm = firmsDao.getById(id);
            if (!firm.getName().equals(targetName)){
                for (PhotosEntity photo :firm.getPhotoList()){
                    for (String photoname : photos){
                        if(photoname.equals(photo.getName())){
                            photosDao.moveFotoToAnotherFirm(photo, firmsDao.getByName(targetName));
                        }
                    }
                }
            }
            firm = firmsDao.getById(id);
            request.setAttribute("firm", firm);
            request.setAttribute("fotoCount", firm.getPhotoList().size());
            RequestDispatcher dispatcher = getServletContext().getRequestDispatcher(SHOW);
            dispatcher.forward(request,response);

            return;
        } else {

            String name = request.getParameter("firmName");
            if (name == null || "".equals(name)) {
                request.setAttribute("firms", firmsDao.getAll());
                RequestDispatcher dispatcher = getServletContext().getRequestDispatcher(LIST);
                dispatcher.forward(request, response);
                return;
            } else {
                if (firmsDao.isExist(name) || firmsDao.addNew(name)) {

                    firm = firmsDao.getByName(name);
                    String uploadFilePath = "E:/java/testpath/temp/";

                    String fileName = null;
                    List<String> fotoNames = new ArrayList<String>();
                    ;
                    //Get all the parts from request and write it to the file on server

                    try {
                        for (Part part : request.getParts()) {
                            fileName = getFileName(part);
                            if (!"".equals(fileName)) {
                                fotoNames.add(fileName);
                                part.write(uploadFilePath + fileName);
                            }
                        }
                        if (!photosDao.addFotosToFirm(firm, fotoNames)) throw new Exception();

                    } catch (Exception e) {
                        e.printStackTrace();
                        String errmessage = "Ошибка добавления фотографий";
                        request.setAttribute("err_message", errmessage);
                        RequestDispatcher dispatcher = getServletContext().getRequestDispatcher(ERROR);
                        dispatcher.forward(request, response);
                        return;
                    }

                    firm = firmsDao.getByName(name);
                    request.setAttribute("firm", firm);
                    request.setAttribute("fotoCount", firm.getPhotoList().size());
                    RequestDispatcher dispatcher = getServletContext().getRequestDispatcher(SHOW);
                    dispatcher.forward(request, response);
                    return;

                } else {

                    String errmessage = "Ошибка добавления новой фирмы";
                    request.setAttribute("err_message", errmessage);
                    RequestDispatcher dispatcher = getServletContext().getRequestDispatcher(ERROR);
                    dispatcher.forward(request, response);
                    return;
                }

            }
        }
    }

    protected void doGet(HttpServletRequest request,
                         HttpServletResponse response) throws ServletException, IOException {
        request.setCharacterEncoding("UTF-8");

        String action = null;
        try {
            action = request.getParameter("action");
        } catch (Exception e) {
            e.printStackTrace();
        }

        if ("new".equalsIgnoreCase(action)){
            RequestDispatcher dispatcher = getServletContext().getRequestDispatcher(NEW);
            dispatcher.forward(request,response);
            return;
        } else if  ("show".equalsIgnoreCase(action)){
            FirmsEntity firm = firmsDao.getById(request.getParameter("id"));
            request.setAttribute("firm", firm);
            request.setAttribute("fotoCount", firm.getPhotoList().size());
            RequestDispatcher dispatcher = getServletContext().getRequestDispatcher(SHOW);
            dispatcher.forward(request,response);
            return;
        } else if  ("edit".equalsIgnoreCase(action)){
            FirmsEntity firm = firmsDao.getById(request.getParameter("id"));
            request.setAttribute("firm", firm);
            request.setAttribute("fotoCount", firm.getPhotoList().size());
            request.setAttribute("firmsList", firmsDao.getAll());
            RequestDispatcher dispatcher = getServletContext().getRequestDispatcher(EDIT);
            dispatcher.forward(request,response);
            return;
        } else if  ("error".equalsIgnoreCase(action)){
            String errmessage = "Ошибка";
            request.setAttribute("err_message", errmessage);
            RequestDispatcher dispatcher = getServletContext().getRequestDispatcher(ERROR);
            dispatcher.forward(request, response);
            return;
        } else {
            request.setAttribute("firms", firmsDao.getAll());
            RequestDispatcher dispatcher = getServletContext().getRequestDispatcher(LIST);
            dispatcher.forward(request,response);
            return;
        }

    }

    private String getFileName(Part part) {
        String contentDisp = part.getHeader("content-disposition");
        System.out.println("content-disposition header= "+contentDisp);
        String[] tokens = contentDisp.split(";");
        for (String token : tokens) {
            if (token.trim().startsWith("filename")) {

                return token.substring(token.indexOf("=") + 2, token.length()-1);
            }
        }
        return "";
    }
}
