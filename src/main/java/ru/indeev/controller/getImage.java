package ru.indeev.controller;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;

/**
 * Created by ferg on 11.04.2017.
 */
@javax.servlet.annotation.WebServlet(name = "getImage", urlPatterns = {"/image"})
public class getImage extends javax.servlet.http.HttpServlet {


    protected void doGet(javax.servlet.http.HttpServletRequest request,
                         javax.servlet.http.HttpServletResponse response) throws javax.servlet.ServletException, IOException {
        request.setCharacterEncoding("UTF-8");
        String imagePath = request.getParameter("imagePath");

        byte[] imageData = Files.readAllBytes(Paths.get(imagePath));
        response.setContentType("image/jpg");
        response.getOutputStream().write(imageData);
        response.getOutputStream().flush();
        response.getOutputStream().close();
    }
}
