package ru.indeev.controller;

import ru.indeev.util.PdfGenerator;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;


/**
 * Created by ferg on 14.04.2017.
 */
@javax.servlet.annotation.WebServlet(name = "getPdf", urlPatterns = {"/getPdf"})
public class getPdf extends javax.servlet.http.HttpServlet {


    protected void doGet(javax.servlet.http.HttpServletRequest request,
                         javax.servlet.http.HttpServletResponse response) throws javax.servlet.ServletException, IOException {
        request.setCharacterEncoding("UTF-8");
        String imagePath = request.getParameter("imagePath");
        System.out.println(imagePath);
        PdfGenerator.fromJpg(imagePath);

        byte[] imageData = Files.readAllBytes(Paths.get("E:/java/testpath/pdf/propusk.pdf"));
        response.setContentType("application/pdf");
        response.getOutputStream().write(imageData);
        response.getOutputStream().flush();
        response.getOutputStream().close();
    }
}