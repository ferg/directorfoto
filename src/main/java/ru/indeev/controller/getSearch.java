package ru.indeev.controller;

import ru.indeev.dao.FirmsDao;
import ru.indeev.dao.PhotosDao;

import ru.indeev.model.PhotosEntity;

import javax.servlet.RequestDispatcher;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by ferg on 14.04.2017.
 */
@javax.servlet.annotation.WebServlet(name = "getSearch", urlPatterns = {"/search"})
public class getSearch extends javax.servlet.http.HttpServlet {

    private static String LIST = "/jsp/firmsList.jsp";
    private static String PHOTOLIST = "/jsp/photoList.jsp";

    private FirmsDao firmsDao;
    private PhotosDao photosDao;

    public getSearch() {
        super();
        this.firmsDao =  new FirmsDao();
        this.photosDao = new PhotosDao();
    }

    protected void doGet(javax.servlet.http.HttpServletRequest request,
                         javax.servlet.http.HttpServletResponse response) throws javax.servlet.ServletException, IOException {

        String action = null;
        request.setCharacterEncoding("UTF-8");
        try {
            action = request.getParameter("action");
        } catch (Exception e) {
            e.printStackTrace();
        }


        if ("lettersearch".equalsIgnoreCase(action)){

            request.setCharacterEncoding("UTF-8");
            String letter = request.getParameter("letter");
            request.setAttribute("firms", firmsDao.searchByFirstLetter(letter));
            RequestDispatcher dispatcher = getServletContext().getRequestDispatcher(LIST);
            dispatcher.forward(request,response);
            return;
        } else if ("allphotos".equalsIgnoreCase(action)){

            List<PhotosEntity> photos = new ArrayList();

            String message = "Все фотографии:";
            photos = photosDao.getAllPhotos();
            request.setAttribute("photos", photos);
            request.setAttribute("photosCount", photos.size());
            request.setAttribute("message", message);
            RequestDispatcher dispatcher = getServletContext().getRequestDispatcher(PHOTOLIST);

            dispatcher.forward(request,response);
            return;
        } else
            {
            RequestDispatcher dispatcher = getServletContext().getRequestDispatcher("/jsp/searchForm.jsp");
            dispatcher.forward(request,response);
        }

    }

    protected void doPost(javax.servlet.http.HttpServletRequest request,
                         javax.servlet.http.HttpServletResponse response) throws javax.servlet.ServletException, IOException {

        request.setCharacterEncoding("UTF-8");
        String action = null;
        try {
            action = request.getParameter("action");
        } catch (Exception e) {
            e.printStackTrace();
        }

        if ("namesearch".equalsIgnoreCase(action)){

            request.setCharacterEncoding("UTF-8");
            String name = request.getParameter("firmName");
            System.out.println(name);
            request.setAttribute("firms", firmsDao.searchByName(name));
            RequestDispatcher dispatcher = getServletContext().getRequestDispatcher(LIST);
            dispatcher.forward(request,response);
            return;
        } else  if ("datesearch".equalsIgnoreCase(action)){

            List<PhotosEntity> photos = new ArrayList();
            String startDate = request.getParameter("startDate");
            String endDate = request.getParameter("endDate");
            String message = "По заданным датам найдено:";
            photos = photosDao.searchByDate(startDate, endDate);
            request.setAttribute("photos", photos);
            request.setAttribute("photosCount", photos.size());
            request.setAttribute("message", message);
            RequestDispatcher dispatcher = getServletContext().getRequestDispatcher(PHOTOLIST);

            dispatcher.forward(request,response);
            return;
        } else {
            RequestDispatcher dispatcher = getServletContext().getRequestDispatcher("/jsp/searchForm.jsp");
            dispatcher.forward(request,response);
        }

    }
}
