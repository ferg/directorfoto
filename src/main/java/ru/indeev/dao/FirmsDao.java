package ru.indeev.dao;



import org.hibernate.Session;
import org.hibernate.Transaction;
import org.hibernate.query.Query;
import ru.indeev.model.FirmsEntity;


import ru.indeev.util.HibernateUtil;

import java.io.File;



import java.sql.Timestamp;
import java.util.List;

/**
 * Created by ferg on 11.04.2017.
 */
public class FirmsDao {


    public boolean addNew(String  firmName){

        Session session = HibernateUtil.getSessionFactory().openSession();

        Transaction transaction = session.beginTransaction();
        FirmsEntity firm = new FirmsEntity();
        firm.setName(firmName);
        firm.setCreatedAt(new Timestamp(System.currentTimeMillis()));

        session.save(firm);

        String path = "E:/java/testpath/" + firm.getName();
        try {
            new File(path).mkdirs();
            transaction.commit();
            session.close();
            return true;
        } catch (Exception e) {
           transaction.rollback();
            session.close();
            return false;
        }

    }



    public FirmsEntity getByName(String firmName){
        Session session = HibernateUtil.getSessionFactory().openSession();
        Query query= session.createQuery("from FirmsEntity where name=:name");
        query.setParameter("name", firmName);
        FirmsEntity firm = (FirmsEntity) query.uniqueResult();
        session.close();
        return firm;
    }

    public FirmsEntity getById(String id){
        Session session = HibernateUtil.getSessionFactory().openSession();
        FirmsEntity firm;
        int idnum = Integer.parseInt(id);
        firm = session.get(FirmsEntity.class, idnum);
        session.close();
        return firm;
    }

    public  boolean isExist(String  firmName){
        Session session = HibernateUtil.getSessionFactory().openSession();
        Query query= session.createQuery("from FirmsEntity where name=:name");
        query.setParameter("name", firmName);
        FirmsEntity firm = (FirmsEntity) query.uniqueResult();
        session.close();
        if (firm==null)return false;
        else return true;
    }

    public  List<FirmsEntity> getAll(){

        Session session = HibernateUtil.getSessionFactory().openSession();
        List<FirmsEntity> firmList = (List<FirmsEntity>) session.createQuery("from FirmsEntity F order by F.name asc ").list();
        session.close();
        return firmList;

    }

    public  List<FirmsEntity> searchByName(String searchName){

        Session session = HibernateUtil.getSessionFactory().openSession();
        List<FirmsEntity> firmList = (List<FirmsEntity>) session.createQuery(
                "from FirmsEntity F where lower(F.name)like lower(:searchName)")
                .setParameter("searchName", "%" + searchName + "%").list();
        session.close();
        return firmList;

    }

    public  List<FirmsEntity> searchByFirstLetter(String letter){

        Session session = HibernateUtil.getSessionFactory().openSession();
        List<FirmsEntity> firmList = (List<FirmsEntity>) session.createQuery(
                "from FirmsEntity F where lower(F.name) like lower(:letter)")
                .setParameter("letter", letter + "%" ).list();
        session.close();
        return firmList;

    }

    public FirmsEntity editName(String id, String targetName){
        Session session = HibernateUtil.getSessionFactory().openSession();
        Transaction transaction = session.beginTransaction();
        FirmsEntity firm;
        int idnum = Integer.parseInt(id);
        firm = session.get(FirmsEntity.class, idnum);
        String path = "E:/java/testpath/" + firm.getName();
        String newpath = "E:/java/testpath/" + targetName;


        try {
                firm.setName(targetName);
                session.update(firm);

                File dir = new File(path);
                dir.renameTo(new File(newpath));

                transaction.commit();
                session.close();
            } catch (Exception e) {
                e.printStackTrace();
                transaction.rollback();
                session.close();
            }

        return firm;

    }

}
