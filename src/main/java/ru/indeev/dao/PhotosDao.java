package ru.indeev.dao;

import com.drew.imaging.ImageMetadataReader;

import com.drew.metadata.Metadata;
import com.drew.metadata.exif.ExifSubIFDDirectory;
import org.hibernate.Session;
import org.hibernate.Transaction;
import org.hibernate.query.Query;
import ru.indeev.model.FirmsEntity;
import ru.indeev.model.PhotosEntity;
import ru.indeev.util.HibernateUtil;
import ru.indeev.util.ThumbnailUtil;

import java.io.File;

import java.nio.file.Paths;
import java.sql.Timestamp;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/**
 * Created by ferg on 12.04.2017.
 */
public class PhotosDao {

    public boolean addFotosToFirm(FirmsEntity firm, List<String> fotoNames){
        Session session = HibernateUtil.getSessionFactory().openSession();
        Transaction transaction = session.beginTransaction();

        try {
            String path = "E:/java/testpath/" + firm.getName() + "/";
            String tempPath = "E:/java/testpath/temp/";
            for(String name: fotoNames){
                PhotosEntity foto = new PhotosEntity();
                if (!PhotosDao.isPhotoExist(firm, name)){

                    foto.setName(name);
                    System.out.println(name);
                    foto.setCreatedAt(new Timestamp(System.currentTimeMillis()));
                    File file = Paths.get(tempPath + name).toFile();

                    Timestamp date = null;
                    try {
                        Metadata metadata = ImageMetadataReader.readMetadata(file);
                        ExifSubIFDDirectory directory = metadata.getFirstDirectoryOfType(ExifSubIFDDirectory.class);
                        date = new Timestamp(directory.getDate(ExifSubIFDDirectory.TAG_DATETIME_ORIGINAL).getTime());

                    } catch (Exception e1) {
                        e1.printStackTrace();
                        date =new Timestamp(System.currentTimeMillis());
                    } finally {
                        ThumbnailUtil.create(path, name, file);
                        file.renameTo(new File(path + name));
                        foto.setShotedAt(date);
                        foto.setFirmName(firm);
                        session.save(foto);

                    }

                }
            }


            transaction.commit();
            session.close();
            return true;
        } catch (Exception e) {
            e.printStackTrace();
            transaction.rollback();
            session.close();
            return false;
        }
    }

    public static boolean isPhotoExist(FirmsEntity firm, String fotoName){
        Session session = HibernateUtil.getSessionFactory().openSession();
        Query query= session.createQuery("from PhotosEntity where name=:name and firmName=:firm");
        query.setParameter("name", fotoName);
        query.setParameter("firm", firm);
        PhotosEntity photo = (PhotosEntity) query.uniqueResult();
        session.close();
        if (photo==null)return false;
        else return true;
    }

    public  List<PhotosEntity> searchByDate(String startDate, String endDate){
        List<PhotosEntity> photos = new ArrayList();
        Session session = HibernateUtil.getSessionFactory().openSession();


        Query query= session.createQuery("from PhotosEntity where shotedAt >=  :startDate and shotedAt <= :endDate");
        if (startDate == null || startDate == "") startDate = "1900-01-01";
        if (endDate == null || endDate == "") endDate = "3000-01-01";

        SimpleDateFormat formatter = new SimpleDateFormat("YYYY-MM-dd");
        Date date1 = null;
        Date date2 = null;
        try {
            date1 = formatter.parse(startDate);
            date2 = formatter.parse(endDate);
        } catch (ParseException e) {
            e.printStackTrace();
        }

        query.setParameter("startDate", date1);
        query.setParameter("endDate", date2);
        photos = (List<PhotosEntity>) query.list();
        session.close();
        return photos;

    }

    public  List<PhotosEntity> getAllPhotos(){
        List<PhotosEntity> photos = new ArrayList();
        Session session = HibernateUtil.getSessionFactory().openSession();
        Query query= session.createQuery("from PhotosEntity");
        photos = (List<PhotosEntity>) query.list();
        session.close();
        return photos;

    }

    public void moveFotoToAnotherFirm(PhotosEntity photo, FirmsEntity targetFirm){
        Session session = HibernateUtil.getSessionFactory().openSession();
        Transaction transaction = session.beginTransaction();

        String path = "E:/java/testpath/" + photo.getFirmName().getName() + "/" + photo.getName();
        String pathTarget = "E:/java/testpath/" + targetFirm.getName() + "/" + photo.getName();
        String thumbPath = "E:/java/testpath/" + photo.getFirmName().getName() + "/thumb/" + photo.getName();
        String thumbPathTarget = "E:/java/testpath/" + targetFirm.getName() + "/thumb/" + photo.getName();

        System.out.println("path:"+ path);
        System.out.println("pathTarget:"+ pathTarget);
        try {
            photo.setFirmName(targetFirm);
            session.update(photo);
            File file = Paths.get(path).toFile();
            File thumbFile = Paths.get(thumbPath).toFile();
            file.renameTo(new File(pathTarget));
            thumbFile.renameTo(new File(thumbPathTarget));
            transaction.commit();
        } catch (Exception e) {
            e.printStackTrace();
            transaction.rollback();
        } finally {
            session.close();
        }

    }
}
