package ru.indeev.model;

import javax.persistence.*;
import java.sql.Timestamp;
import java.util.List;

/**
 * Created by ferg on 11.04.2017.
 */
@Entity
@Table(name = "firms", schema = "public", catalog = "directorfoto")
public class FirmsEntity {
    private int id;
    private String name;
    private Timestamp createdAt;
    private List<PhotosEntity> photoList;

    @Id
    @SequenceGenerator(name="firms_id_seq", sequenceName="firms_id_seq", allocationSize=1)
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator="firms_id_seq")
    @Column(name = "id", nullable = false)
    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    @Basic
    @Column(name = "name", nullable = false, length = 50)
    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    @Basic
    @Column(name = "created_at", nullable = true)
    public Timestamp getCreatedAt() {
        return createdAt;
    }

    public void setCreatedAt(Timestamp createdAt) {
        this.createdAt = createdAt;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        FirmsEntity that = (FirmsEntity) o;

        if (id != that.id) return false;
        if (name != null ? !name.equals(that.name) : that.name != null) return false;
        if (createdAt != null ? !createdAt.equals(that.createdAt) : that.createdAt != null) return false;

        return true;
    }

    @Override
    public int hashCode() {
        int result = id;
        result = 31 * result + (name != null ? name.hashCode() : 0);
        result = 31 * result + (createdAt != null ? createdAt.hashCode() : 0);
        return result;
    }

    @OneToMany(mappedBy = "firmName", fetch = FetchType.EAGER)
    public List<PhotosEntity> getPhotoList() {
        return photoList;
    }

    public void setPhotoList(List<PhotosEntity> photoList) {
        this.photoList = photoList;
    }
}
