package ru.indeev.model;

import javax.persistence.*;
import java.sql.Timestamp;

/**
 * Created by ferg on 11.04.2017.
 */
@Entity
@Table(name = "photos", schema = "public", catalog = "directorfoto")
public class PhotosEntity {
    private int id;
    private String name;
    private Timestamp shotedAt;
    private Timestamp createdAt;
//    private int firmId;
    private FirmsEntity firmName;

    @Id
    @SequenceGenerator(name="photos_id_seq", sequenceName="photos_id_seq", allocationSize=1)
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator="photos_id_seq")
    @Column(name = "id", nullable = false)
    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    @Basic
    @Column(name = "name", nullable = false, length = 50)
    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    @Basic
    @Column(name = "shoted_at", nullable = true)
    public Timestamp getShotedAt() {
        return shotedAt;
    }

    public void setShotedAt(Timestamp shotedAt) {
        this.shotedAt = shotedAt;
    }

    @Basic
    @Column(name = "created_at", nullable = true)
    public Timestamp getCreatedAt() {
        return createdAt;
    }

    public void setCreatedAt(Timestamp createdAt) {
        this.createdAt = createdAt;
    }

//    @Basic
//    @Column(name = "firm_id", nullable = false)
//    public int getFirmId() {
//        return firmId;
//    }
//
//    public void setFirmId(int firmId) {
//        this.firmId = firmId;
//    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        PhotosEntity that = (PhotosEntity) o;

        if (id != that.id) return false;
//        if (firmId != that.firmId) return false;
        if (name != null ? !name.equals(that.name) : that.name != null) return false;
        if (shotedAt != null ? !shotedAt.equals(that.shotedAt) : that.shotedAt != null) return false;
        if (createdAt != null ? !createdAt.equals(that.createdAt) : that.createdAt != null) return false;

        return true;
    }

    @Override
    public int hashCode() {
        int result = id;
        result = 31 * result + (name != null ? name.hashCode() : 0);
        result = 31 * result + (shotedAt != null ? shotedAt.hashCode() : 0);
        result = 31 * result + (createdAt != null ? createdAt.hashCode() : 0);
//        result = 31 * result + firmId;
        return result;
    }

    @ManyToOne
    @JoinColumn(name = "firm_id", referencedColumnName = "id", nullable = false)
    public FirmsEntity getFirmName() {
        return firmName;
    }

    public void setFirmName(FirmsEntity firmName) {
        this.firmName = firmName;
    }
}
