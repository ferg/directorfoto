package ru.indeev.util;

import com.itextpdf.text.*;
import com.itextpdf.text.pdf.PdfWriter;
import java.io.FileOutputStream;
import java.net.URL;

/**
 * Created by ferg on 14.04.2017.
 */
public class PdfGenerator {

    private static String FILE = "E:/java/testpath/pdf/propusk.pdf";

    public static void fromJpg(String path){

        Document document = new Document();
        Rectangle one = new Rectangle(280,540);
        document.setMargins(60, 60, 60, 60);
        document.setPageSize(one);

        try {

            PdfWriter writer = PdfWriter.getInstance(document, new FileOutputStream(FILE));
            document.open();
            document.add(new Paragraph(" "));

            Image image = Image.getInstance(path);
            image.scaleAbsolute(114, 85);
            document.add(image);
            document.add(new Paragraph(" "));
            document.add(new Paragraph(" "));
            document.add(new Paragraph(" "));
            document.add(new Paragraph(" "));
            document.add(image);
            document.close();
            writer.close();

        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
