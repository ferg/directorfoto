package ru.indeev.util;

import org.imgscalr.Scalr;

import javax.imageio.ImageIO;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;

/**
 * Created by ferg on 15.04.2017.
 */
public class ThumbnailUtil {

    public static void create(String path, String fileName, File file) throws IOException{
        String thumbPath = path + "thumb/";
        new File(thumbPath).mkdirs();
        File thumbFile = new File(thumbPath + fileName);

        BufferedImage image = ImageIO.read(file);
        BufferedImage scaledImage = Scalr.resize(image, 300);
        ImageIO.write(scaledImage,"jpg", thumbFile);
    }
}
