<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@page pageEncoding="UTF-8" %>

<%@include file="/jsp/header.jsp"%>
<div  class="container">

    <div>
        <h2><c:out value="${firm.name}"/></h2>

        <form action='/firm'  method='POST'>
            <div class="form-group"  >
                <label for='name'> Название: </label>

                <input type="text" name="firmName" id='name' value='<c:out value="${firm.name}"/>'>
                <input type="text" name="action" value="editFirmName" style="display: none;"/>
                <input type="text" name="id" value="<c:out value="${firm.id}"/>" style="display: none;"/>
            </div>

            <div class="form-group">
                <button type="submit" class="btn btn-default"> Изменить название! </button>
            </div>

        </form>


        <form action='/firm' method="post">
            <input type="text" name="action" value="movePhoto" style="display: none;"/>

        <c:forEach var="i" begin="0" end="${fotoCount}" step="4">
            <div class="row" style="padding-bottom: 20px;">

                <c:forEach items="${firm.photoList}" var="foto" begin="${i}" end="${i+3}" step="1">

                    <div class="col-md-3" style="text-align:center;">


                        <img class="foto" src="/image?imagePath=E:/java/testpath/<c:out value="${firm.name}"/>/thumb/<c:out value="${foto.name}"/>"  width="95%"  >
                        <input type="checkbox" name="foto" value="<c:out value="${foto.name}"/>" /><c:out value="${foto.name}"/> <br />


                    </div>

                </c:forEach>

            </div>

        </c:forEach>

            <p><select size="10"  name="targetName">
                <option disabled>Выберите фирму</option>
                <c:forEach items="${firmsList}" var="targetfirm">
                    <option value="<c:out value="${targetfirm.name}"/>"><c:out value="${targetfirm.name}"/></option>
                </c:forEach>
            </select></p>

            <input type="text" name="id" value="<c:out value="${firm.id}"/>" style="display: none;"/>

            <input type="submit" class="btn btn-default" value="Переместить выбранные фото!" />

        </form>

    </div>






</div>


<%@include file="/jsp/footer.jsp"%>
