<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@page pageEncoding="UTF-8" %>

<%@include file="/jsp/header.jsp"%>
<div  class="container">

    <h2> Список зарегистрированных фирм:</h2>
    <ul>

        <c:forEach items="${firms}" var="firm">

            <li><a href="/firm?action=show&id=<c:out value="${firm.id}"/>"> <c:out value="${firm.name}"/> </a> </li>

        </c:forEach>
    </ul>



</div>


<%@include file="/jsp/footer.jsp"%>
