
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@page pageEncoding="UTF-8" %>

<html>
<head>
    <title>Фото на пропуск</title>
    <link rel="icon" type="image/png" href="../static/nstar.png" >
    <link rel="stylesheet" type="text/css" href="../static/css/bootstrap.min.css">
    <link rel="stylesheet" type="text/css" href="../static/css/bootstrap-theme.min.css">
    <link rel="stylesheet" href="../static/css/jquery-ui.min.css">

    <script src="../static/js/jquery.min.js"> </script>
    <script src="../static/js/bootstrap.min.js"></script>
    <script src="../static/js/app.js"></script>
    <script src="../static/js/jquery-ui.min.js"></script>

    <meta charset="utf-8">

</head>
<body>

<%--<!-- header -->--%>
<nav class=" navbar navbar-default" >
    <div class="container">
        <div class="navbar-header">
            <a class="navbar-brand" href="/">Фото на пропуск</a>
        </div>
        <ul class="nav navbar-nav">
            <li ><a href="/firm">Список фирм</a></li>
            <li><a href="/firm?action=new">Добавить фирму</a></li>
            <li ><a href="/search">Поиск</a></li>
            <li><a href="/search?action=allphotos">Показать все фото</a></li>
        </ul>
        <ul class="nav navbar-nav navbar-right">
            <li><a href="#"><span class="glyphicon glyphicon-user"></span> Sign Up</a></li>
            <li><a href="#"><span class="glyphicon glyphicon-log-in"></span> Login</a></li>
        </ul>
    </div>
</nav>

<nav aria-label="Page navigation">
    <div  class="container">
        <ul class="pagination">
            <% String[] alphabet = {"А","Б","В","Г","Д","Е","Ё","И","Й","К","Л","М","Н","О","П","Р","С","Т","У","Ф","Х","Ц","Ч","Ш","Щ","Ъ","Ы","Ь","Э","Ю","Я"}; %>
            <% for (String letter : alphabet) { %>

            <li><a href="/search?action=lettersearch&letter=<%= letter %>" ><%= letter %></a></li>
            <% } %>


        </ul>
    </div>
</nav>

