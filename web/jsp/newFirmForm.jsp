
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@page pageEncoding="UTF-8" %>

<%@include file="/jsp/header.jsp"%>
<div  class="container">

    <h2> Добавить новую фирму </h2>
    <form action='/firm' enctype="multipart/form-data" method='POST' accept-charset="UTF-8">
        <div class="form-group">
            <label for='name'> Название: </label>
            <input type="text" name="firmName" id='name'>
        </div>
        <div class="form-group">
            <label for='fotofile'> Добавить фото: </label>
            <input name="foto" type="file" id='fotofile' multiple>
        </div>
        <div class="form-group">
            <button type="submit" class="btn btn-default"> Добавить </button>
        </div>




    </form>



</div>



<%@include file="/jsp/footer.jsp"%>