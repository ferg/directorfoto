<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@page pageEncoding="UTF-8" %>

<%@include file="/jsp/header.jsp"%>
<div  class="container">

    <div>
        <h2><c:out value="${message}"/></h2>


        <c:forEach var="i" begin="0" end="${photosCount}" step="4">
            <div class="row" style="padding-bottom: 20px;">

                <c:forEach items="${photos}" var="foto" begin="${i}" end="${i+3}" step="1">

                    <div class="col-md-3" style="text-align:center;">

                        <a href="/image?imagePath=E:/java/testpath/<c:out value="${foto.firmName.name}"/>/<c:out value="${foto.name}"/>">
                        <img class="foto" src="/image?imagePath=E:/java/testpath/<c:out value="${foto.firmName.name}"/>/thumb/<c:out value="${foto.name}"/>"  width="95%"  >
                        </a>
                        <button class="btn btn-default" type="submit" data-nomer="15 " onclick="window.open('/getPdf?imagePath=E:/java/testpath/<c:out value="${foto.firmName.name}"/>/<c:out value="${foto.name}"/>')">
                            <span class="glyphicon glyphicon-print" aria-hidden="true"></span>
                            Печать пропуска
                        </button>

                    </div>

                </c:forEach>

            </div>

        </c:forEach>

    </div>





</div>


<%@include file="/jsp/footer.jsp"%>
