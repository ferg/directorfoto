<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@page pageEncoding="UTF-8" %>

<%@include file="/jsp/header.jsp"%>
<div  class="container">

<h3> Поиск по названию фирмы</h3>


        <form action="/search" method="POST" accept-charset="UTF-8">
            <div class="form-group">
                <label for="name"> Название фирмы: </label>
                <input type="text" name="firmName" id="name">
                <input type="text" name="action" value="namesearch" style="display: none;" >
            </div>

            <div class="form-group">
                <button type="submit" class="btn btn-default"> Искать </button>
            </div>
        </form>


    <h3>Поиск по дате съемки фото</h3>

    <form action="/search" method="POST">

        <div class="form-group " >
            <label for="datepicker1"> сфототографировано после: </label>
            <input name="startDate" type="text"  id="datepicker1" size="18">
            <label for="datepicker2"> или до: </label>
            <input name="endDate"  type="text" id="datepicker2" size="18">
            <input type="text" name="action" value="datesearch" style="display: none;" >
        </div>
        <div class="form-group">
            <button type="submit" class="btn btn-default"> Искать </button>
        </div>
    </form>

</div>


<%@include file="/jsp/footer.jsp"%>